<?php

namespace App\Models;

use Exception;
use CodeIgniter\Model;
use CodeIgniter\Database\Exceptions\DatabaseException;

class NewsModel extends Model
{
  protected $db;

  public function __construct(&$db)
  {
    $this->db =& $db;
  }


  /*
    cikk rogzitese a tablaba az adatok ellenorzesevel
  */
  function addNews($post)
  {
    $errors = $result = array();
    // ellenorizzuk a bejovo adatokat
    // whitespace eltavolitasa
    $post['title'] = empty($post['title']) ? null : trim($post['title']);
    if(empty($post['title']))
    {
      $errors['title'] = 'Nem adott meg címet!';
    }
    // html kodok eltavolitasa
    $post['content'] = empty($post['content']) ? null : strip_tags(trim($post['content']));
    if(empty($post['content']))
    {
      $errors['content'] = 'Nem adott meg tartalmat!';
    }
    $post['keywords'] = empty($post['keywords']) ? null : trim($post['keywords']);
    if(empty($errors))
    {
      // milyen hosszu a tartalom
      $characters = mb_strlen($post['content'], 'utf-8');
      // szavak szama
      $words = str_word_count($post['content']);
      // felbontjuk a kulcsszavakat es letisztitjuk
      $keywords = array();
      $tmp_keywords = explode(' ', mb_strtolower($post['keywords'],'utf-8'));

      // az '' szavakat eldobjuk
      foreach($tmp_keywords as $keyword)
      {
        if(!empty($keyword))
        {
          $keywords []= $keyword;
        }
      }

      try
      {
        // egy tranzakcioban frissitjuk a tablakat
        $this->db->query('BEGIN');
        // beszurjuk a cikket a tablaba
        $this->db->query('INSERT INTO news (title,content,keywords,characters,words,created_date,created_time)
                          VALUES ('.$this->db->escape($post['title']).','
                                   .$this->db->escape($post['content']).','
                                   .$this->db->escape(empty($post['keywords']) ? null : $post['keywords']).','
                                   .$this->db->escape($characters).','
                                   .$this->db->escape($words).','
                                   .'DATE(NOW()),TIME(NOW()))');
        $result['id_news'] = $this->db->insertID();
        if(empty($result['id_news']))
        {
          // nem sikerult az insert
          throw new Exception('Nem sikerült létrehozni a hírt az adatbázisban');
        }
        // ha van kulcsszo, akkor eloszor hozzaadjuk a globalis listahoz az ujakat
        if(!empty($keywords))
        {
          // insertelheto listat keszitunk a kulcsszavakbol
          $keywordsInsert = array_map(function ($str) { return '('.$this->db->escape($str).')'; },$keywords);
          $this->db->query('INSERT IGNORE keywords (keyword) VALUES '.implode(',', $keywordsInsert ));
          // hozzakapcsoljuk a kulcsszavakat a hirhez
          $this->db->query('INSERT INTO news_keywords
                                SELECT '.$this->db->escape($result['id_news']).',id_keyword FROM keywords '
                                .'WHERE keyword in ('.implode(',', array_map( array($this->db,'escape'), $keywords ) ).')');
        }
        $this->db->query('COMMIT');
      }
      catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
      {
        $this->db->query('ROLLBACK');
        $errors['message'] = $e->getMessage().' ['.$e->getCode().']';
      }
      catch (\Exception $e)
      {
        $this->db->query('ROLLBACK');
        $errors['message'] = $e->getMessage().' ['.$e->getCode().']';
      }
    }
    $result['errors'] = $errors;
    $result['post'] = $post;
    return $result;
  }

  /*
   Mennyi hir szerepel a tablaban
  */
  function getNewsCount()
  {
    try
    {
      $query = $this->db->query('SELECT count(*) AS total FROM news');
      $row = $query->getRowArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return empty($row['total']) ? 0 : (int)$row['total'];
  }

  /*
    A megadott oldal hireit adja vissza parameter szerint sorrendben
  */
  function getNewsList($start = 0, $offset = 10, $order = 'date_desc')
  {
    $result = array();
    $order = explode('_', strtolower($order));
    // leellenorizzuk a rendezesi iranyt, ha nincsen akkor a default 'desc'
    $sortDir = empty($order[1]) ? 'desc' : (in_array($order[1], array('asc','desc')) ? $order[1] : 'desc');

    // rendezesi mezo ellenorzese es beallitasa, a default 'date'
    $orderCol = empty($order[0]) ? 'date' : (in_array($order[0], array('date','title')) ? $order[0] : 'date');

    // segedtomb a rendezes beallitasahoz
    $columns = array('date' => 'created_date '.$sortDir.', created_time '.$sortDir,
                     'title' => 'title '.$sortDir);
    try
    {
      // a parameter alapjan lekerdezzuk a kivalasztott oldalon szereplo cikkeket
      $query = $this->db->query('SELECT title,content,created_date,created_time, keywords FROM news
                  ORDER BY '.$columns[$orderCol].' LIMIT '.(int)$start.','.(int)$offset);
      $result = $query->getResultArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return $result;
  }

  /*
   a 10 leggyakrabben elofordulo cimke listaja
  */
  function getTop10Keywords()
  {
    $result = array();
    try
    {
      // a belso selectben meghatarozzuk a 10 leggyakoribb cimket,
      // majd csak erre a 10-re joinoljuk a cimke megnevezeset
      $query = $this->db->query('SELECT a.id_keyword,b.keyword,a.db FROM
              (
               SELECT count(*) AS db,id_keyword FROM news_keywords
               GROUP BY id_keyword ORDER BY db DESC LIMIT 10
              ) a
              INNER JOIN keywords b ON a.id_keyword = b.id_keyword
              ORDER BY db DESC, keyword ASC');
      $result = $query->getResultArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return $result;
  }

  /*
    a leghosszabb cikk(ek) listaja
  */
  function getLongestNews()
  {
    $result = array();
    try
    {
      // lekerdezzuk a legtobb szot tartalmazo cikket
      // tobb is lehet a valaszban, ekkor a regebbi lesz elol
      $query = $this->db->query('SELECT * FROM news
                                 WHERE words = (SELECT MAX(words) FROM news)
                                 ORDER BY id_news DESC');
      $result = $query->getResultArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return $result;
  }

  /*
    napi atlagos cikk karakterszam lekerdezese
  */
  function getDailyCharacterCount()
  {
    $result = array();
    try
    {
      // naponkent atlagoljuk a felvitt cikkek karakterszamat
      $query = $this->db->query('SELECT created_date,AVG(characters) as char_avg FROM news
                                 GROUP BY created_date
                                 ORDER BY created_date');
      $result = $query->getResultArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return $result;
  }

  /*
    a naponta letrehozott cikkek szama
  */
  function getDailyNewsCount()
  {
    $result = array();
    try
    {
      // naponkent osszesitjuk a felvitt cikkek szamat
      $query = $this->db->query('SELECT created_date,COUNT(*) as db FROM news
                                 GROUP BY created_date
                                 ORDER BY created_date');
      $result = $query->getResultArray();
    }
    catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    catch (\Exception $e)
    {
      return $e->getMessage().' ['.$e->getCode().']';
    }
    return $result;
  }

}
