<?php namespace App\Controllers;


class Home extends BaseController
{
	public function index($page = 1, $order = null)
	{
		$data = array();
		$sessionOrder = $this->session->get('newsOrder');
		if(!empty($order))
		{
			$this->session->set('newsOrder', $order);
		}
		else
		{
			$order = $sessionOrder;
		}
		$newsModel = new \App\Models\NewsModel($this->db);
		$newsCount = $newsModel->getNewsCount();
		if(is_int($newsCount))
		{
			$pageSize = 10;
			$pages = ceil($newsCount / $pageSize);
			$page = empty($page) ? 1 : (int)$page;
			$page = max(min($page, $pages),1);
			$start = ($page - 1) * $pageSize;
			$newsPage = $newsModel->getNewsList($start, $pageSize, $order);
			if(is_array($newsPage))
			{
				$data['newsPage'] = $newsPage;
				$data['pages'] = $pages;
				$data['page'] = $page;
				$data['order'] = $order;
			}
			else
			{
				$data['error'] = $newsPage;
			}
		}
		else
		{
			$data['error'] = $newsCount;
		}
		return view('index', $data);
	}

	//--------------------------------------------------------------------

	public function addNews()
	{
		$newsModel = new \App\Models\NewsModel($this->db);
		$data = array();
		if($_POST)
		{
			$result = $newsModel->addNews($_POST);
			if(!empty($result['id_news']))
			{
				return redirect()->to('/');
			}
			else
			{
				$data['post'] = $result['post'];
				$data['errors'] = $result['errors'];
			}
		}
		return view('addNews', $data);
	}

	public function statistics()
	{
		$newsModel = new \App\Models\NewsModel($this->db);
		$data = array();

		$top10 = $newsModel->getTop10Keywords();
		if(is_array($top10))
		{
			$data['top10'] = $top10;
		}
		else
		{
			$data['errorTop10'] = $top10;
		}

		$longestNews = $newsModel->getLongestNews();
		if(is_array($longestNews))
		{
			$data['longestNews'] = $longestNews;
		}
		else
		{
			$data['errorLongestNews'] = $longestNews;
		}

		$dailyCharCount = $newsModel->getDailyCharacterCount();
		if(is_array($dailyCharCount))
		{
			$data['dailyCharCount'] = $dailyCharCount;
		}
		else
		{
			$data['errorDailyCharCount'] = $dailyCharCount;
		}

		$dailyNewsCount = $newsModel->getDailyNewsCount();
		if(is_array($dailyNewsCount))
		{
			$data['dailyNewsCount'] = $dailyNewsCount;
		}
		else
		{
			$data['errorDailyNewsCount'] = $dailyNewsCount;
		}

		return view('statistics', $data);
	}

}
