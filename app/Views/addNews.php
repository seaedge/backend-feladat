<?= $this->extend('Views/layout') ?>

<?= $this->section('content') ?>
<main class="container mt-2">
  <div class="starter-template text-center py-5">
    <h1 class="py-3">Új hír rögzítése</h1>
    <?php if(!empty($errors['message'])): ?>
    <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
      <?php echo $errors['message']; ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif; ?>
    <form class="text-start" action="<?php echo current_url(); ?>" method="post" accept-charset="utf-8">
      <div class="form-group mb-2">
        <label for="formGroupExampleInput">Cím</label>
        <input type="text" name="title" class="form-control<?php echo empty($post) ? '' : (empty($errors['title']) ? ' is-valid' : ' is-invalid'); ?>" id="title" placeholder="Cím">
        <?php if(!empty($errors['title'])): ?>
          <div class="invalid-feedback"><?php echo $errors['title']; ?></div>
        <?php endif; ?>
      </div>
      <div class="form-group mb-2">
        <label for="formGroupExampleInput2">Tartalom</label>
        <textarea name="content" class="form-control<?php echo empty($post) ? '' : (empty($errors['title']) ? ' is-valid' : ' is-invalid'); ?>" rows="8" id="content" placeholder="Lorem ipsum..."></textarea>
        <?php if(!empty($errors['content'])): ?>
          <div class="invalid-feedback"><?php echo $errors['content']; ?></div>
        <?php endif; ?>
      </div>
      <div class="form-group mb-2">
        <label for="formGroupExampleInput2">Kulcsszavak</label>
        <input type="text" name="keywords" class="form-control" id="keywords" placeholder="">
      </div>
      <div class="form-group text-center">
        <button type="submit" name="submit" class="btn btn-primary">Ok</button>
        <a href="/" class="btn btn-outline-secondary">Mégsem</a>
      </div>
    </form>
  </div>
</main>
<?= $this->endSection('content') ?>
