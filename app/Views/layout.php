<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title>Hírek modul</title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/all.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
  </head>
  <body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Hírek modul</a>
    <ul class="m-0 float-end">
    <a class="btn btn-light" href="/addNews">Új hír rögzítése</a>
    <a class="btn btn-light" href="/statistics">Statisztikák</a>
    </ul>
  </div>
</nav>

  <?= $this->renderSection('content') ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function()
  {
      $('#newsOrder').on('change', function(e)
      {
        top.location = $(this).data('url')+$(this).val();
      });
   });

  </script>
  </body>
</html>
