<?= $this->extend('Views/layout') ?>

<?= $this->section('content') ?>
<main class="container mt-2">
  <div class="starter-template py-5">
    <h1 class="py-3 text-center">Hírek</h1>
    <?php if(!empty($error)): ?>
    <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
      <?php echo $error; ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif; ?>
    <?php if(empty($newsPage)): ?>
      <p>Nincs megjeleníthető hír</p>
    <?php else: ?>

    <div class="row">
      <div class="col-xl-4 offset-xl-8">
        <div class="row">
          <label class="form-label col-xl-4" for="newsOrder">Rendezés:</label>
          <div class="col-xl-8">
            <select class="form-control w150" data-url="/page/1/" name="newsOrder" id="newsOrder">
              <option value="date_desc"<?php echo ((empty($order) OR $order == 'date_desc') ? ' selected="selected"' : ''); ?>>Dátum szerint csökkenő</option>
              <option value="date_asc"<?php echo ((!empty($order) AND $order == 'date_asc') ? ' selected="selected"' : ''); ?>>Dátum szerint növekvő</option>
              <option value="title_desc"<?php echo ((!empty($order) AND $order == 'title_desc') ? ' selected="selected"' : ''); ?>>Cím szerint csökkenő</option>
              <option value="title_asc"<?php echo ((!empty($order) AND $order == 'title_asc') ? ' selected="selected"' : ''); ?>>Cím szerint növekvő</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <?php foreach($newsPage as $news): ?>

    <h2 class="mt-2"><?php echo $news['title']; ?></h2>
    <p><strong><?php echo $news['created_date'].' '.$news['created_time']; ?></strong></p>
    <p><u><?php echo $news['keywords']; ?></u></p>
    <p><?php echo nl2br($news['content']); ?></p>
    <hr />
    <?php endforeach; ?>

    <?php if($pages > 1): ?>
    <div class="py-5 text-center">
      <a class="btn btn-<?php echo $page == 1 ? 'secondary' : 'primary'; ?>" href="/page/1">1</a>
      <?php if($page > 5): ?>
      <span>...</span>
      <?php endif; ?>
      <?php $current = max(2,$page - 3); $steps = 3; ?>
      <?php while($current < $page AND $steps > 0): ?>
      <a class="btn btn-primary" href="/page/<?php echo $current; ?>"><?php echo $current;?></a>
      <?php $current++; $steps--; ?>
      <?php endwhile; ?>

      <?php if(!in_array($page, array(1,$pages))): ?>
      <a class="btn btn-secondary" href="/page/<?php echo $page; ?>"><?php echo $page;?></a>
      <?php endif; ?>

      <?php $current = $page + 1; $steps = 3; ?>
      <?php while($current < $pages AND $steps > 0): ?>
      <a class="btn btn-primary" href="/page/<?php echo $current; ?>"><?php echo $current;?></a>
      <?php $current++; $steps--; ?>
      <?php endwhile; ?>

      <?php if($page < ($pages - 4)): ?>
      <span>...</span>
      <?php endif; ?>
      <a class="btn btn-<?php echo $page == $pages ? 'secondary' : 'primary'; ?>" href="/page/<?php echo $pages; ?>"><?php echo $pages;?></a>
    </div>
    <?php endif; ?>
    <?php endif; ?>
  </div>
</main>
<?= $this->endSection('content') ?>
