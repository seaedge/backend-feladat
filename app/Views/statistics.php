<?= $this->extend('Views/layout') ?>

<?= $this->section('content') ?>
<main class="container mt-2">
  <div class="starter-template py-5">
    <h1 class="py-3 text-center">Statisztikák</h1>

    <h2 class="mt-4">Top10 cimke</h2>
    <?php if(!empty($errorTop10)): ?>
    <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
      <?php echo $errorTop10; ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif; ?>
    <?php if(!empty($top10)): ?>
      <ul>
      <?php foreach($top10 as $idx => $item): ?>
        <li><?php echo '<strong>'.($idx+1).'. '.$item['keyword'].'</strong> ('.$item['db'].' db cikk)'; ?></li>
      <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <h2 class="mt-4">Legtöbb szót tartalmazó cikk(ek)</h2>
    <?php if(!empty($errorLongestNews)): ?>
    <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
      <?php echo $errorLongestNews; ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif; ?>
    <?php if(!empty($longestNews)): ?>
      <?php foreach($longestNews as $news): ?>
        <h3 class="mt-2"><?php echo $news['title'].' ('.$news['words'].' szó)'; ?></h3>
        <p><strong><?php echo $news['created_date'].' '.$news['created_time']; ?></strong></p>
        <p><u><?php echo $news['keywords']; ?></u></p>
        <p><?php echo nl2br($news['content']); ?></p>
        <hr />
      <?php endforeach; ?>
    <?php endif; ?>
    
    <div class="row">
      <div class="col-xl-6">
        <h2 class="mt-4">Napi átlagos cikk karakterszám</h2>
        <?php if(!empty($errorDailyCharCount)): ?>
        <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
          <?php echo $errorDailyCharCount; ?>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php endif; ?>
        <?php if(!empty($dailyCharCount)): ?>
          <ul>
          <?php foreach($dailyCharCount as $item): ?>
            <li><?php echo $item['created_date'].': <strong>'.floor($item['char_avg']).'</strong> karakter'; ?></li>
          <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>

      <div class="col-xl-6">
        <h2 class="mt-4">Naponta rögzített cikkek</h2>
        <?php if(!empty($errorDailyNewsCount)): ?>
        <div class="alert alert-danger alert-dismissible fade show text-start" role="alert">
          <?php echo $errorDailyNewsCount; ?>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php endif; ?>
        <?php if(!empty($dailyNewsCount)): ?>
          <ul>
          <?php foreach($dailyNewsCount as $item): ?>
            <li><?php echo $item['created_date'].': <strong>'.$item['db'].'</strong> db cikk'; ?></li>
          <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</main>
<?= $this->endSection('content') ?>
